import { Client, Module, ModuleManifest } from "discord-modules";
import { TemplateCommand } from "commands/TemplateCommand"; 
import { TemplateListener } from "listeners/TemplateListener";

export default class TemplateModule extends Module {
    constructor(client: Client, manifest: ModuleManifest) {
        super(client, manifest);
        console.info("INFO: Constructed discord-module-template.");
    }

    public async register(): Promise<void> {
        console.info("INFO: Registered discord-module-template.");

        await Promise.all([
            this.client.guildCommandManager.register(new TemplateCommand(this)),
            this.client.listenerManager.register(new TemplateListener(this))
        ]).catch(console.error);

        return;
    }
}